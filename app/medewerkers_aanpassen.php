<?php include "includes/header.php" ?>

<?php
$id = $_GET['id'];

$query = "SELECT 
			 id,
			 voornaam,
			 achternaam,
			 afdeling,
			 toestelnummer,
			 portret
		FROM
			medewerkers 
		WHERE id = " . intval($id) . ";";
	
$result = mysqli_query($conn, $query) or die (mysqli_error($conn));
$medewerker = mysqli_fetch_assoc($result);

?>

<div class="container">
	<div class="row">
		<div class="col-12">

			<h1>Medewerker aanpassen</h1>

			<form method="post" action="medewerker_update.php">
				<div class="form-group">
					<input type="hidden" name="id" placeholder="id" value="<?php echo $medewerker['id']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Voornaam</label>
					<input type="text" name="voornaam" placeholder="voornaam" value="<?php echo $medewerker['voornaam']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Achternaam</label>
					<input type="text" name="achternaam" placeholder="achternaam" value="<?php echo $medewerker['achternaam']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Afdeling</label>
					<input type="text" name="afdeling" placeholder="afdeling" value="<?php echo $medewerker['afdeling']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Toestelnummer</label>
					<input type="text" name="toestelnummer" placeholder="toestelnummer" value="<?php echo $medewerker['toestelnummer']?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Portret</label>
					<input type="text" name="portret" placeholder="portret" value="<?php echo $medewerker['portret']?>" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="medewerkers_tonen.php" role="button">annuleren</a>
					<input type="submit" value="opslaan" class="btn btn-success">
				</div>
			</form>

		</div>
	</div>
</div>


<?php include "includes/footer.php" ?>