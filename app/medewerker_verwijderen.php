<?php include "includes/header.php" ?>

<?php
$id = $_GET['id'];

$query 	= "SELECT 
			 id,
			 voornaam,
			 achternaam,
			 afdeling,
			 toestelnummer,
			 portret
		FROM
			medewerkers 
		WHERE id = " . intval($id) . ";";
	

$result = mysqli_query($conn, $query) or die (mysqli_error($conn));
$medewerker = mysqli_fetch_assoc($result);

?>


<div class="container">
	<div class="row">
		<div class="col-12">

			<h1>Medewerker verwijderen</h1>
			<p>Weet u zeker dat u deze medewerker wilt verwijderen?</p>

			<table class="table">
				<?php foreach ($medewerker as $key => $value): ?>
			    <tr>
		        <th><?=$key?></th>
		        <td><?=$value?></td>
			    </tr>
				<?php endforeach; ?>
			</table>

			<form method="post" action="medewerker_delete.php">
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $medewerker['id']?>" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="medewerkers_tonen.php" role="button">annuleren</a>
					<input type="submit" value="verwijder" class="btn btn-primary">
				</div>
			</form>

		</div>
	</div>
</div>

<?php include "includes/footer.php" ?>