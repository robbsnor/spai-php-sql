<?php include "includes/header.php" ?>

<?php

$query = "SELECT * FROM tosti";

$result = mysqli_query($conn, $query)
or die (mysqli_error($conn));

?>

 
 <div class="container">
 	<div class="row">
 		<div class="col-12">
			<!-- check of er rows in de tabel zijn ingevoerd -->
			<?php if (mysqli_num_rows($result) > 0){ ?>
			
 			<table class="table">
 				<thead>
 					<th>ID</th>
 					<th>Voornaam</th>
 					<th>Achternaam</th>
 					<th>Afdeling</th>
 					<th>Toestelnummer</th>
 					<th>Portret</th>
 				</thead>

 				<tbody>
 					<?php while ($row = mysqli_fetch_assoc($result)){ ?>
 						<tr>
 							<th> <?php echo $row['id']; ?> </th>
 							<td> <?php echo $row['voornaam']; ?> </td>
 						    <td> <?php echo $row['achternaam']; ?> </td>
 						    <td> <?php echo $row['afdeling']; ?> </td>
 							<td> <?php echo $row['toestelnummer']; ?> </td>
 						    <td> <img src="<?php echo $row['portret']; ?>" alt="" width="25px" height="auto"> </td>
 						</tr>
 					 
 					<?php }; ?>
 				</tbody>

 			</table>
 			 
 			<?php } else { ?>
 				<p class="warning">Geen tosti's gevonden...</p>
 			<?php }; ?>

 		</div>
 	</div>
 </div>

<?php include "includes/footer.php" ?>