<?php include "includes/header.php" ?>

 <div class="container">
 	<div class="row">
 		<div class="col-12">

			<h1>Medewerker toevoegen</h1>

			<form method="post" action="medewerker_insert.php">
				<div class="form-group">
					<label>Voornaam</label>
					<input type="text" name="voornaam" placeholder="voornaam" class="form-control">
				</div>
				<div class="form-group">
					<label>Achternaam</label>
					<input type="text" name="achternaam" placeholder="achternaam" class="form-control">
				</div>
				<div class="form-group">
					<label>Afdeling</label>
					<input type="text" name="afdeling" placeholder="afdeling" class="form-control">
				</div>
				<div class="form-group">
					<label>Toestelnummer</label>
					<input type="text" name="toestelnummer" placeholder="toestelnummer" class="form-control">
				</div>
				<div class="form-group">
					<label>Portret</label>
					<input type="text" name="portret" placeholder="portret image url" class="form-control">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="medewerkers_tonen.php" role="button">annuleren</a>
					<input type="submit" value="toevoegen" class="btn btn-success">
				</div>
			</form>

 		</div>
 	</div>
 </div>

<?php include "includes/footer.php" ?>