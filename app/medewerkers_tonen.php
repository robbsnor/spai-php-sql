<?php include "includes/header.php" ?>

<?php

$query = "SELECT * FROM medewerkers;";

$result = mysqli_query($conn, $query)
or die (mysqli_error($conn));

?>

<div class="container">
	<div class="row">
		<div class="col-12">

			<h1>Overzicht</h1>
			<p><a href="medewerker_toevoegen.php">Nieuwe medewerker toevoegen</a></p>

			<?php if (mysqli_num_rows($result) > 0): ?>
			 
			<table class="table">
				<tr>
					<th>id</th>
					<th>voornaam</th>
					<th>achternaam</th>
			        <th>afdeling</th>
					<th>toestelnummer</th>
			        <th>portret</th>
			        <th></th>
			        <th></th>
				</tr>
				<?php while ($row = mysqli_fetch_assoc($result)): ?>
				<tr>
					<td><?php echo $row['id']; ?></td>
					<td><?php echo $row['voornaam']; ?></td>
					<td><?php echo $row['achternaam']; ?></td>
					<td><?php echo $row['afdeling']; ?></td>
					<td><?php echo $row['toestelnummer']; ?></td>
				    <td><img src="<?php echo $row['portret']; ?>" alt="" width="auto" height="20px"></td>
				    <td><a href="medewerkers_aanpassen.php?id=<?php echo $row['id']; ?>">bewerken</a>
			    	<td><a href="medewerker_verwijderen.php?id=<?php echo $row['id']; ?>">verwijderen</a>
				</tr>
			<?php endwhile; ?>
			</table>
			 
			<?php else: ?>
			<p class="warning">Geen medewerkers gevonden...</p>
			<?php endif; ?>

		</div>
	</div>
</div>


<?php include "includes/footer.php" ?>