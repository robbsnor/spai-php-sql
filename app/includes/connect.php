<?php

// stap 1: verbinding met de MySQL server maken
$conn = mysqli_connect('localhost', 'root', '')
or die('kan geen verbinding met database maken');

// stap 2: set charset specificeren
mysqli_set_charset($conn, 'utf8')
or die('charset kan niet correct worden ingesteld');

// stap 3: database selecteren
mysqli_select_db($conn, 'lerensql')
or die('database niet beschikbaar');

?>
