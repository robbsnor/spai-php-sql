-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 18 okt 2017 om 08:44
-- Serverversie: 10.1.26-MariaDB
-- PHP-versie: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lerensql`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `medewerkers`
--

CREATE TABLE `medewerkers` (
  `id` tinyint(4) NOT NULL,
  `voornaam` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `achternaam` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `afdeling` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `toestelnummer` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `portret` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `medewerkers`
--

INSERT INTO `medewerkers` (`id`, `voornaam`, `achternaam`, `afdeling`, `toestelnummer`, `portret`) VALUES
(14, 'Kees', 'Verbeek', 'IT', '2134', 'https://avatars0.githubusercontent.com/u/8747855?v=4&s=460'),
(15, 'Henk', 'Kaas', 'Design', '5313', 'https://avatars0.githubusercontent.com/u/8747855?v=4&s=460'),
(16, 'Freek', 'Willem', 'IT', '1354', 'https://avatars0.githubusercontent.com/u/8747855?v=4&s=460'),
(17, 'Peter', 'Appel', 'Design', '1346', ''),
(18, 'Sjon', 'Kewl', 'Design', '9985', ''),
(19, 'Jan', 'de Boer', 'IT', '8947', ''),
(20, 'Eric', 'Bier', 'Design', '5674', ''),
(21, 'Norma', 'Pizza', 'Design', '5557', ''),
(22, 'Geert', 'Henk', 'IT', '6731', ''),
(23, '2D', 'Gorillaz', 'IT', '8974', '');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `medewerkers`
--
ALTER TABLE `medewerkers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `medewerkers`
--
ALTER TABLE `medewerkers`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
